syntax = "proto3";

package energy;
option go_package = "genproto/energy_management";

service EnergyManagementService {
  rpc GetCityDailyConsumption(GetCityDailyConsumptionRequest) returns (GetCityDailyConsumptionResponse);
  rpc GetBuildingHourlyConsumption(GetBuildingHourlyConsumptionRequest) returns (GetBuildingHourlyConsumptionResponse);
  rpc SubmitMeterReading(SubmitMeterReadingRequest) returns (SubmitMeterReadingResponse);
  rpc GetGridSectorStatus(GetGridSectorStatusRequest) returns (GetGridSectorStatusResponse);
  rpc ReportPowerOutage(ReportPowerOutageRequest) returns (ReportPowerOutageResponse);
  rpc GetEnergySavingTips(GetEnergySavingTipsRequest) returns (GetEnergySavingTipsResponse);
  rpc EnrollInDemandResponseProgram(EnrollInDemandResponseProgramRequest) returns (EnrollInDemandResponseProgramResponse);
  rpc GetUpcomingDemandResponseEvents(GetUpcomingDemandResponseEventsRequest) returns (GetUpcomingDemandResponseEventsResponse);
  rpc RegisterRenewableEnergySource(RegisterRenewableEnergySourceRequest) returns (RegisterRenewableEnergySourceResponse);
}

message GetCityDailyConsumptionRequest {
  string date = 1;
}

message GetCityDailyConsumptionResponse {
  double total_consumption_kwh = 1;
  repeated HourlyConsumption hourly_breakdown = 2;
}

message HourlyConsumption {
  int32 hour = 1;
  double consumption_kwh = 2;
}

message GetBuildingHourlyConsumptionRequest {
  string building_id = 1;
  string date = 2;
}

message GetBuildingHourlyConsumptionResponse {
  repeated HourlyConsumption hourly_consumption = 1;
}

message SubmitMeterReadingRequest {
  string meter_id = 1;
  double reading_value = 2;
  string timestamp = 3;
}

message SubmitMeterReadingResponse {
  bool success = 1;
  string message = 2;
}

message GetGridSectorStatusRequest {
  string sector_id = 1;
}

message GetGridSectorStatusResponse {
  string status = 1;
  double current_load_percentage = 2;
}

message ReportPowerOutageRequest {
  string sector_id = 1;
  string description = 2;
  Point location = 3;
}

message Point {
  double latitude = 1;
  double longitude = 2;
}

message ReportPowerOutageResponse {
  bool success = 1;
  string message = 2;
  string outage_id = 3;
}

message GetEnergySavingTipsRequest {
  string citizen_id = 1;
}

message EnergySavingTip {
  string tip = 1;
  double estimated_savings_kwh = 2;
}

message GetEnergySavingTipsResponse {
  repeated EnergySavingTip tips = 1;
}

message EnrollInDemandResponseProgramRequest {
  string citizen_id = 1;
  string program_id = 2;
}

message EnrollInDemandResponseProgramResponse {
  bool success = 1;
  string message = 2;
}

message GetUpcomingDemandResponseEventsRequest {
  string citizen_id = 1;
}

message DemandResponseEvent {
  string event_id = 1;
  string start_time = 2;
  string end_time = 3;
  string description = 4;
}

message GetUpcomingDemandResponseEventsResponse {
  repeated DemandResponseEvent events = 1;
}

message RegisterRenewableEnergySourceRequest {
  string building_id = 1;
  string source_type = 2;
  double capacity_kw = 3;
  string installation_date = 4;
}

message RegisterRenewableEnergySourceResponse {
  bool success = 1;
  string message = 2;
  string source_id = 3;
}